// prefer default export if available
const preferDefault = m => m && m.default || m

exports.components = {
  "component---src-templates-blog-default-js": () => import("../src/templates/blog-default.js" /* webpackChunkName: "component---src-templates-blog-default-js" */),
  "component---src-templates-tutorial-default-js": () => import("../src/templates/tutorial-default.js" /* webpackChunkName: "component---src-templates-tutorial-default-js" */),
  "component---src-templates-resume-default-js": () => import("../src/templates/resume-default.js" /* webpackChunkName: "component---src-templates-resume-default-js" */),
  "component---cache-dev-404-page-js": () => import("dev-404-page.js" /* webpackChunkName: "component---cache-dev-404-page-js" */),
  "component---src-pages-about-js": () => import("../src/pages/about.js" /* webpackChunkName: "component---src-pages-about-js" */),
  "component---src-pages-blog-js": () => import("../src/pages/blog.js" /* webpackChunkName: "component---src-pages-blog-js" */),
  "component---src-pages-contact-js": () => import("../src/pages/contact.js" /* webpackChunkName: "component---src-pages-contact-js" */),
  "component---src-pages-index-js": () => import("../src/pages/index.js" /* webpackChunkName: "component---src-pages-index-js" */),
  "component---src-pages-portfolio-js": () => import("../src/pages/portfolio.js" /* webpackChunkName: "component---src-pages-portfolio-js" */),
  "component---src-pages-research-js": () => import("../src/pages/research.js" /* webpackChunkName: "component---src-pages-research-js" */),
  "component---src-pages-resume-js": () => import("../src/pages/resume.js" /* webpackChunkName: "component---src-pages-resume-js" */),
  "component---src-pages-tutorials-js": () => import("../src/pages/tutorials.js" /* webpackChunkName: "component---src-pages-tutorials-js" */)
}

