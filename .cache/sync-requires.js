const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---src-templates-blog-default-js": hot(preferDefault(require("/Users/teresavasquez/Sites/playground/gitlab/ultimate-portfolio/src/templates/blog-default.js"))),
  "component---src-templates-tutorial-default-js": hot(preferDefault(require("/Users/teresavasquez/Sites/playground/gitlab/ultimate-portfolio/src/templates/tutorial-default.js"))),
  "component---src-templates-resume-default-js": hot(preferDefault(require("/Users/teresavasquez/Sites/playground/gitlab/ultimate-portfolio/src/templates/resume-default.js"))),
  "component---cache-dev-404-page-js": hot(preferDefault(require("/Users/teresavasquez/Sites/playground/gitlab/ultimate-portfolio/.cache/dev-404-page.js"))),
  "component---src-pages-about-js": hot(preferDefault(require("/Users/teresavasquez/Sites/playground/gitlab/ultimate-portfolio/src/pages/about.js"))),
  "component---src-pages-blog-js": hot(preferDefault(require("/Users/teresavasquez/Sites/playground/gitlab/ultimate-portfolio/src/pages/blog.js"))),
  "component---src-pages-contact-js": hot(preferDefault(require("/Users/teresavasquez/Sites/playground/gitlab/ultimate-portfolio/src/pages/contact.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/Users/teresavasquez/Sites/playground/gitlab/ultimate-portfolio/src/pages/index.js"))),
  "component---src-pages-portfolio-js": hot(preferDefault(require("/Users/teresavasquez/Sites/playground/gitlab/ultimate-portfolio/src/pages/portfolio.js"))),
  "component---src-pages-research-js": hot(preferDefault(require("/Users/teresavasquez/Sites/playground/gitlab/ultimate-portfolio/src/pages/research.js"))),
  "component---src-pages-resume-js": hot(preferDefault(require("/Users/teresavasquez/Sites/playground/gitlab/ultimate-portfolio/src/pages/resume.js"))),
  "component---src-pages-tutorials-js": hot(preferDefault(require("/Users/teresavasquez/Sites/playground/gitlab/ultimate-portfolio/src/pages/tutorials.js")))
}

