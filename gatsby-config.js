const path = require('path')

module.exports = {
    pathPrefix: "/drtwebsite",
    plugins: [
        {
            resolve: 'gatsby-plugin-root-import',
            options: {
                // create an alias for each path you would use
                // use 'root/...'
                root: path.join(__dirname, '/'),
                
                // use 'src/...'
                src: path.join(__dirname, './src'),

                // use 'styles/...'
                styles: path.join(__dirname, './src/assets/styles'),
                
                // use 'components/...'
                components: path.join(__dirname, './src/components'),
                
                // use 'layouts/...'
                layouts: path.join(__dirname, './src/layouts'),
            }
        },

        // FILE CONTROL
        //content that displays on generated pages
        {
            resolve: 'gatsby-source-filesystem',
            options: {
            path: `${__dirname}/src/pageContent`,
            name: 'pageContent',
            },
        },

        // FILE CONTROL
        //content that displays on resumes
        {
            resolve: 'gatsby-source-filesystem',
            options: {
            path: `${__dirname}/src/resumeContent`,
            name: 'resumeContent',
            },
        },

        //dynamic content includes admin generated files
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                path: `${__dirname}/src/dynamicContent`,
                name: 'dynamicContent',
            },
        },

        //dynamic content includes admin generated files
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                path: `${__dirname}/src/settings`,
                name: 'settings',
            },
        },

        // for header
        `gatsby-plugin-react-helmet`,

        //JSON for data
        'gatsby-transformer-json',

        // IMAGE CONTROL
        'gatsby-plugin-sharp',
        'gatsby-transformer-sharp',
        
         // SASS/CSS
        'gatsby-plugin-sass',

        // CMS
        // `gatsby-plugin-netlify-cms`,
        // With Options:
        // CMS


        // set the admin login path
        {
            resolve: 'gatsby-plugin-netlify-cms',
            options: {
                modulePath: `${__dirname}/src/cms/cms.js`,
                publicPath: `polymath/`,
                htmlTitle: `Dr. T's Website Admin`
            },
        }
    ],
  }
