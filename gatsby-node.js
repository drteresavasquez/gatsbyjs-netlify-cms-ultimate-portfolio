const _ = require('lodash')
const path = require('path')
const fs = require('fs')
const { createFilePath } = require('gatsby-source-filesystem')

require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`,
})

/*****************************************************************************/
/********************** SLUG CREATION FOR DYNAMIC PAGES **********************/
/*****************************************************************************/

// TODO: For every dynamically created item, create a `dynamicNodes` object for the set, create the graphQL query, then add to `dataSet` in "Generate Dynamic Posts from GraphQL Query" section

const dynamicSlugNodes = [
  // type = results from console.log(node.internal.type);
  // basePath = ONLY the parent directory that contains the files 
  // NOTE: no slashes on basePath
  {type: `BlogJson`, basePath: `blog`},
  {type: `TutorialsJson`, basePath: `tutorials`},
  {type: `ResumeJson`, basePath: `resume`}
]
exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions
  // console.log(node.internal.type)

  dynamicSlugNodes.forEach((item)=> {
    if (node.internal.type === `${item.type}`) {
      const slug = createFilePath({ node, getNode, basePath: `${item.basePath}`})
      createNodeField({
        node,
        name: `slug`,
        value: slug,
      })
    }
  })
}

/************************************************************/
/********************** GENERATE PAGES **********************/
/************************************************************/

exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions

  // Query from data files found
  return graphql(`
    {
      
        allBlogJson {
            edges {
                node {
                    id
                    templateKey
                    title
                    fields {
                    slug
                    }
                }
            }
        }

        allTutorialsJson {
            edges {
                node {
                    id
                    templateKey
                    title
                    fields {
                    slug
                    }
                }
            }
        }

        allResumeJson {
            edges {
                node {
                    id
                    templateKey
                    title
                    fields {
                    slug
                    }
                }
            }
        }

    }
  `).then(result => {
        if (result.errors) {
          result.errors.forEach(e => console.error(e.toString()))
          return Promise.reject(result.errors)
        }

    /************* Generate Dynamic Posts from GraphQL Query ************/

    // TODO: Add dynamic dataset and url prefix 
    const dataSet = [
      // no opening or trailing slashes on url
      {posts: result.data.allBlogJson.edges, url: 'blog'},
      {posts: result.data.allTutorialsJson.edges, url: 'tutorial'},
      {posts: result.data.allResumeJson.edges, url: 'resume'}
    ]

    const makeDynamicPages = ( posts, urlPath ) => {
      posts.forEach(edge => {
        const id = edge.node.id
          createPage({
            path: `${urlPath}${edge.node.fields.slug}`,
            tags: edge.node.tags,
            component: path.resolve(`./src/templates/${String(edge.node.templateKey)}.js`),
            // additional data can be passed via context
            context: {
              id,
              slug: edge.node.fields.slug,
            },
          })
        })
    }

    dataSet.forEach((object) => makeDynamicPages(object.posts, object.url));

    // /************* Generate tags from Blog JSON files ************/

    // const blogPosts = result.data.allBlogJson.edges

    // // Tag pages:
    // let tags = []
    // // Iterate through each post, putting all found tags into `tags`
    // blogPosts.forEach(edge => {
    //   if (_.get(edge, `node.tags`)) {
    //     tags = tags.concat(edge.node.tags)
    //   }
    // })
    // // Eliminate duplicate tags
    // tags = _.uniq(tags)

    // // Make tag pages
    // tags.forEach(tag => {
    //   const tagPath = `tags/${_.kebabCase(tag)}/`

    //   createPage({
    //     path: tagPath,
    //     component: path.resolve(`./src/global_elements/templates/tags.js`),
    //     context: {
    //       tag,
    //     },
    //   })
    // })

    return "all done";
  }).then((result)=>{console.log(result);})
}