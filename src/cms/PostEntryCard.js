import { Link as ReactRouterLink } from 'react-router-dom'
import CMS from 'netlify-cms'

const formatDate = date => {
  if (date && date.toLocaleDateString) return date.toLocaleDateString('en')
  return date.toString ? date.toString() : ''
}

const PostEntryCard = ({entry, collection}) => {
  const title = entry.getIn(['data', 'title'])
  const date = entry.getIn(['data', 'date'])
  const slug = entry.get('slug')
  const path = `/collections/${collection.get('name')}/entries/${slug}`
  return (
    <div style={{flex: 1, minWidth: '100%'}}>
      <ReactRouterLink to={path}>
        <h2>{title}</h2>
        <h3>{formatDate(date)}</h3>
      </ReactRouterLink>
    </div>
  )
}

CMS.registerEntryCard('posts', PostEntryCard)