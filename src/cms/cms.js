import CMS from "netlify-cms-app";
import * as ColorWidget from "netlify-cms-widget-color";

import ButtonStylesPreview from 'src/preview-templates/ButtonStylesPreview'

// REGISTERED WIDGETS
CMS.registerWidget("color", ColorWidget.Control);

//PREVIEW STYLES 
// CMS.registerPreviewStyle('/styles.css')

//PREVIEW TEMPLATES
// CMS.registerPreviewTemplate('blog', BlogPostPreview)
CMS.registerPreviewTemplate('buttons', ButtonStylesPreview)
