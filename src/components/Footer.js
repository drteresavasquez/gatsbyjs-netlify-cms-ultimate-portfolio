import React, { Component } from 'react'

export default class Footer extends Component {
  state = {}

  render() {

    return (
      <div className="footer">
        <div className="footer-column-wrapper">
          <div className="footer-column">Column 1</div>
          <div className="footer-column">Column 2</div>
          <div className="footer-column">Column 3</div>
          <div className="footer-column">Column 4</div>
        </div>
      </div>
    )
  }
}

