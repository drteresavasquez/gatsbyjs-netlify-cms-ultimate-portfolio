import React from 'react';
import { Jumbotron, Button } from 'reactstrap';

const Hero = (props) => {
  return (
    <div>
      <Jumbotron>
        <h1>{props.title}</h1>
      </Jumbotron>
    </div>
  );
};

export default Hero;