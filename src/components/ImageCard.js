import React from 'react';
import { Card, CardTitle, CardText, CardImg, CardImgOverlay, Button } from 'reactstrap';

const ImageCard = (props) => {
  return (
    <div>
      <Card inverse>
        <CardImg width="100%" src={`https://via.placeholder.com/${props.width}x${props.height}/123456/666666`} alt="Card image cap" />
        <CardImgOverlay>
          <CardTitle>{props.title}</CardTitle>
          <CardText>
              {props.description}
          </CardText>
          <Button color="primary" size="sm" href={props.prefix + props.link}>Small Button</Button>{' '}
                <Button color="secondary" size="sm">Small Button</Button>
          <CardText>
            <small className="text-muted">Last updated 3 mins ago</small>
          </CardText>
        </CardImgOverlay>
      </Card>
    </div>
  );
};

export default ImageCard;