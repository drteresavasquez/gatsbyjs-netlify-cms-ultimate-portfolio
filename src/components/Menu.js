import React from 'react';
import { 
  FaGithub, 
  FaLinkedin, 
  FaFacebookSquare, 
  FaTwitterSquare,
  FaYoutubeSquare } from "react-icons/fa";

export default class Example extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: "collapse"
    };
  }
  toggle() {
    if (this.state.isOpen === "collapse") {
      this.setState({
        isOpen: ""
      });
    } else {
      this.setState({
        isOpen: "collapse"
      });
    }
    
  }
  render() {
    return (
      <nav className="navbar sticky-top navbar-expand-lg navbar-dark primary-bg">
        <a className="navbar-brand" href="/">
          <img src="" alt="LOGO" className="d-inline-block align-top" />
        </a>
        <button onClick={this.toggle} className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className={`${this.state.isOpen} navbar-collapse`} id="navbarNav">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <a className="nav-link" href="/blog/">Blog <span className="sr-only">(current)</span></a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/tutorials/">Tutorials</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/about/">My Story</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/research/">Research</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/contact/">Contact</a>
            </li>
          </ul>

          <span className="navbar-text">
            <ul className="navbar-nav">
              <li className="nav-item">
                <a className="nav-link" href="/resume/">Resume</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/portfolio/">Portfolio</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="https://github.com/drteresavasquez/" target="_blank" rel="noopener noreferrer"><FaGithub /></a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="https://linkedin.com/in/drteresavasquez/" target="_blank" rel="noopener noreferrer"><FaLinkedin /></a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="https://facebook.com/drteresavasquez" target="_blank" rel="noopener noreferrer"><FaFacebookSquare /></a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="https://www.youtube.com/channel/UC9eaQhH0UppGZZLY85oXRUQ" target="_blank" rel="noopener noreferrer"><FaYoutubeSquare /></a>
              </li>
              <li className="nav-item last-item">
                <a className="nav-link" href="https://twitter.com/drteresavasquez/" target="_blank" rel="noopener noreferrer"><FaTwitterSquare /></a>
              </li>
              {/* <li className="nav-item hide-on-mobile">
                <form className="form-inline">
                  <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                  <button className="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
                </form>
              </li> */}
            </ul>
           
          </span>
        </div>
      </nav>
     
    );
  }
}
