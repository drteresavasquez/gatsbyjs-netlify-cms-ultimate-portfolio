import React from 'react';
import { Jumbotron, Container } from 'reactstrap';

const PageTitleHero = (props) => {
  return (
    <div>
      <Jumbotron fluid>
        <Container fluid>
          <div className="jumbotron-heading-container">
            <h1>{props.title}</h1>
            <p className="lead">{props.subheading}</p>
          </div>
        </Container>
      </Jumbotron>
    </div>
  );
};

export default PageTitleHero;