import React from 'react'
import Menu from './../components/Menu'
import Footer from './../components/Footer'
import 'styles/main.scss'
import 'bootstrap/dist/css/bootstrap.min.css';
import Helmet from 'react-helmet';


export default ({ children }) => {
    return(
        <React.Fragment>
            <Helmet>
            <meta name="apple-mobile-web-app-title" content="Dr. T Web" />
            <meta name="apple-mobile-web-app-capable" content="yes" />
            <meta name="apple-mobile-web-app-status-bar-style" content="white" />

            </Helmet>
            <Menu />
                <div>
                    {children}
                </div>
                <div className="push"></div>
            <Footer />
        </React.Fragment>
    )
}