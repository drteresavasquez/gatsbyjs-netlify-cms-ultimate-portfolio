import React from "react"
import MainLayout from 'src/layouts/Main'
import PageTitleHero from 'components/PageTitleHero'

export default () => {
    return(
        <MainLayout>
            <PageTitleHero title="Page Title" />
            <div className="content-container">Here is some content</div>
        </MainLayout>
    )
}
