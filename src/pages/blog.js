import React from "react"
import MainLayout from 'src/layouts/Main'
import ImageCard from 'components/ImageCard'
import { graphql } from 'gatsby'

export default ({data}) => {
    const posts = data.allBlogJson.edges
    const pageContent = data.allPageContentJson.edges[0].node

    const cards = posts.map((post, i)=>{
        return(
            <ImageCard
                key={i}
                title={post.node.title}
                description={post.node.description}
                prefix="/blog"
                link={`${post.node.fields.slug}`}
                width="900"
                height="250"
            />
        )
    })
    return(
    <MainLayout>
        <div>{pageContent.headline}</div>
        <div className="card-deck">
            {cards}
        </div>
    </MainLayout>
    )
}


export const pageQuery = graphql`
  query {
    allPageContentJson(filter: {templateKey: {eq: "blog"}}){
      edges {
        node {
          headline
        }
      }
    }

    allBlogJson (sort: { fields: [date], order: DESC }) {
      edges{
        node{
          title
          date
          body
          description
          fields {
            slug
          }
        }
      }
    }


  }
`