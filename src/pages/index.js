import React from "react"
import MainLayout from 'src/layouts/Main'
import ImageCard from 'components/ImageCard'
import Hero from 'components/Hero'
import { graphql } from 'gatsby'

export default ({data}) => {
    const tutorials = data.allTutorialsJson.edges
    const pageContent = data.allPageContentJson.edges[0]
    console.log(pageContent);

    const cards = tutorials.map((tutorials, i)=>{        
        return(
            <ImageCard
                key={i}
                title={tutorials.node.title}
                description={tutorials.node.description}
                prefix="/tutorial"
                link={`${tutorials.node.fields.slug}`}
                width="300"
                height="400"
            />
        )
    })
    return(
    <MainLayout>
        <Hero 
            title={pageContent.node.headline}
        />
        <div className="card-deck">
            {cards}
        </div>
    </MainLayout>
    )
}

export const pageQuery = graphql`
  query {
    allPageContentJson(filter: {templateKey: {eq: "home"}}){
      edges {
        node {
          headline
        }
      }
    }

    allTutorialsJson(limit:3) {
        edges {
            node {
                title
                date
                body
                description
                fields {
                    slug
                }
            }
        }
    }


  }
`