import React from "react"
import MainLayout from 'src/layouts/Main'
import PageTitleHero from 'components/PageTitleHero'
import ResumeCard from 'components/ResumeCard'
import { graphql } from 'gatsby'
import Moment from 'react-moment'
import get from 'lodash/get'

export default class Resume extends React.Component {
  state = {
    loggedIn: false,
  }

  componentDidMount() {
    const validated = localStorage.getItem('key');
    if (validated) {
      this.setState({
        loggedIn: true
      })
    }
  }
  render() {

    const __ = get(this, 'props.data.allPageContentJson.edges[0].node')
    const resumes = get(this, 'props.data.allResumeJson.edges')
 
    const cards = resumes.map((resume, i)=> {
      const status = resume.node.status === "No Longer Interested" ||
                     resume.node.status === "Rejected" ? "red" : resume.node.status === "Resume Sent" ? "yellow" : resume.node.status === "Interview Scheduled" || resume.node.status === "Interviewed" ? "green" : null

      return(
        <ResumeCard
          key={i}
          title={resume.node.title}
          prefix="/resume"
          link={`${resume.node.fields.slug}`}
          submittedTo={resume.node.submittedTo}
          postionAppliedFor={resume.node.postionAppliedFor}
          notes={resume.node.notes}
          statusColor={status}
          statusText={resume.node.status}
          dateApplied={ 
            <Moment format="MM/DD/YYYY">
              {resume.node.dateApplied}
            </Moment>
          }
        />
      )
    })

  const handleChange = (event) => {
    event.preventDefault();
    const value = event.target.value;
    if (value === "123456") {
      localStorage.setItem('key', true);
      this.setState({loggedIn: true});
    }
  }

  const DOMState = () => {
    if (this.state.loggedIn) {
      return ( 
        <>
          <PageTitleHero 
              title={__.headline}
              subheading={__.subheading}
            />
          <div className="content-container">
            {cards}
          </div>
        </>
        )
     } else {
      return ( 
        <>
          <PageTitleHero 
                title="Please Log In"
          />

          <input
            type="password"
            name="password"
            placeholder="*********"
            id="password"
            required={true}
            onChange={handleChange}
          />
        </>
        )
     }
  }

  return(
    <MainLayout>
      {DOMState()}
    </MainLayout>
  )
  }
}

export const pageQuery = graphql`
  query {
    allPageContentJson(filter: {templateKey: {eq: "resume"}}){
      edges {
        node {
          headline
          subheading
        }
      }
    }

    allResumeJson (sort: { fields: [dateApplied], order: DESC }) {
      edges {
        node {
          title
          submittedTo
          postionAppliedFor
          notes
          dateApplied
          status
          fields {
            slug
          }
        }
      }
    }
  }
`
