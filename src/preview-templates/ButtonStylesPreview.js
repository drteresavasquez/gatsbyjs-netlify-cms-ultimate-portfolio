import React from 'react'
import PropTypes from 'prop-types'
import MainLayout from 'layouts/Main'

const ButtonPreview = ({ entry, widgetFor }) => {

  const rStyles = entry.getIn(['data', 'button-styles']);

  // console.log('data', entry.getIn(['data', 'buttons', 'primary']));
  
  // const colors = [
  //   {backgroundColor: `${entry.getIn(['data', 'colors', 'primary'])}`, color: 'white', width: '300px', padding: '25px', margin: '25px', cursor: "pointer"}, 
  //   {backgroundColor: `${entry.getIn(['data', 'colors', 'secondary'])}`, color: 'white', width: '300px', padding: '25px', margin: '25px', cursor: "pointer"},
  //   {backgroundColor: `${entry.getIn(['data', 'colors', 'tertiary'])}`, color: 'white', width: '300px', padding: '25px', margin: '25px', cursor: "pointer"},
  //   {backgroundColor: `${entry.getIn(['data', 'colors', 'white'])}`, color: 'white', width: '300px', padding: '25px', margin: '25px', cursor: "pointer"}
  // ]

  // const text = [
  //   `${entry.getIn(['data', 'text', 'primary-text'])}`,
  //   `${entry.getIn(['data', 'text', 'secondary-text'])}`, 
  //   `${entry.getIn(['data', 'text', 'tertiary-text'])}`, 
  //   `${entry.getIn(['data', 'text', 'white-text'])}` 
  // ]
  
  const buttons = 
  // colors.map((color, i) => {
    // return
    (
      <button>{entry.getIn(['data', 'primary', 'backgroundColor'])}</button>
    )
  // }
  // )
  return(
    <MainLayout>
      <h2>{entry.getIn(['data', 'title'])}</h2>
      <p>{entry.getIn(['data', 'intro'])}</p>
      {buttons}
    </MainLayout>
  )
}


ButtonPreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
  widgetFor: PropTypes.func,
}

export default ButtonPreview