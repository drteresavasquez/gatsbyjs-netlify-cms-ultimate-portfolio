import Markdown from 'react-markdown'
import { graphql } from 'gatsby'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import MainLayout from 'layouts/Main'
class BlogPostTemplate extends Component {
  render() {
    return (
      <MainLayout>
        <section className="section">
          <div className="content-container">
            <div className="columns">
              <div className="column is-10 is-offset-1">
                <h1 className="title is-size-2 has-text-weight-bold is-bold-light">
                  {this.props.title}
                </h1>
                <h3>{this.props.date}</h3>
                <p>{this.props.description}</p>
                <Markdown source={this.props.content} />
              </div>
            </div>
          </div>
        </section>
      </MainLayout>
    )
  }
}

BlogPostTemplate.propTypes = {
  content: PropTypes.string.isRequired,
  contentComponent: PropTypes.func,
  description: PropTypes.string,
  title: PropTypes.string
}


const BlogPost = ({ data }) => {
  const { allTutorialsJson: tutorial } = data
  return (
    <BlogPostTemplate
      content={tutorial.edges[0].node.body}
      date={tutorial.edges[0].node.date}
      description={tutorial.edges[0].node.description}
      title={tutorial.edges[0].node.title}
    />
  )
}

export default BlogPost

export const query = graphql`
  query($slug: String!) {
    allTutorialsJson(filter: { fields: { slug: {eq: $slug }}}){
      edges {
          node {
            id
            templateKey
            title
            description
            body
            date(formatString: "MMMM DD, YYYY")
          }
        }
      }
  }
`